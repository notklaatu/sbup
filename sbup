#!/usr/bin/env python3

#
# This is a hack job for easily version bumping a SlackBuild/info.
#

# TODO
# checking upstream signatures would be nice
# support multiline SlackBuilds (md5s and downloads)
# check if it's outdated according to repology

import argparse
import sys
import os
import subprocess
import re
import hashlib
from urllib.parse import urlparse
# from pprint import pprint

# get command line args
parser = argparse.ArgumentParser(description="""sbup lets you version bump
SlackBuilds easily.""")
parser.add_argument('version', type=str, nargs='?',
                    help="Specify the update version.")
args = parser.parse_args()

# check for a version argument
if args.version:
    latest = args.version
else:
    print("Supply an update version number.")
    sys.exit()


# some colours
class colour:
    purple = '\033[95m'
    cyan = '\033[96m'
    darkcyan = '\033[36m'
    blue = '\033[94m'
    green = '\033[92m'
    yellow = '\033[93m'
    red = '\033[91m'
    bold = '\033[1m'
    underline = '\033[4m'
    end = '\033[0m'


# FUNCTIONS
def is_tool(name):
    "Check whether `name` is in $PATH and is executable."
    from shutil import which
    return which(name) is not None


def md5(fname):
    "Generate an MD5 hash."
    if not os.path.isfile(fname):
        print("Not a valid file: " + fname)
        return False
    hash_md5 = hashlib.md5()
    with open(fname, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def xstr(s):
    "Convert a None to a string."
    if s is None:
        return ''
    return str(s)


def is_url(url):
    "Check if a URL is valid (enough)."
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


# stolen from https://stackoverflow.com/a/3041990
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}

    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        try:
            sys.stdout.write(question + prompt)
            choice = input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'yes' or 'no' "
                                 "(or 'y' or 'n').\n")
        except KeyboardInterrupt:
            sys.exit()


def download(url):
    "Try to download a file with wget."
    if not is_url(url):
        print("Bad URL:" + url + "\nCan't update.")
        sys.exit()

    print("Try to download: " + url)
    try:
        wget = subprocess.run(['wget', '--continue', url])
        if wget.returncode == 0:
            return True
        else:
            print("Download failed (wget error code: " +
                  str(wget.returncode) + ')')
            return False
    except KeyboardInterrupt:
        print("User interrupt.")
        sys.exit()


# FIND INFO FILE
def read_info():
    "Find and read an .info in current working dir."
    # get first .info file found
    files = os.listdir(os.getcwd())
    try:
        # TODO choose between multiple .info files
        # search for all .info files
        info_fnames = list(filter(lambda x: re.search(r'\.info$', x), files))
        # create dictionary to store slackbuild .info details
        # (filename, contents, etc).
        info_dict = {}
        # for now just take the first .info file found
        info_dict['fname'] = info_fnames[0]
    except IndexError:
        return None
    # read it in
    with open(info_dict['fname']) as file:
        info_dict['contents'] = file.read()
        return info_dict


# check for necessary external programs
if not is_tool('wget'):
    print("Can't find wget.")
    sys.exit()
if not is_tool('sdiff'):
    print("Can't find sdiff.")
    sys.exit()

# info dictionary
info = read_info()
if info is None:
    print("Can't find .info file.")
    sys.exit()
else:
    # add updated version info
    info['new_version'] = latest
    # get corresponding SlackBuild
    info['build_fname'] = re.sub('.info$', '', info['fname']) + '.SlackBuild'
    build_fname = info['build_fname']
    # exit if there's no SlackBuild for it.
    if not os.path.isfile(build_fname):
        print("Can't find " + build_fname)
        sys.exit()

# PARSE .info
# regexps to do
rx_dict = {
    'prgnam': re.compile(r'PRGNAM=\"(.*?)\"'),
    'version': re.compile(r'VERSION=\"(.*?)\"'),
    'homepage': re.compile(r'HOMEPAGE=\"(.*?)\"'),
    'requires': re.compile(r'REQUIRES=\"(.*?)\"'),
    'maintainer': re.compile(r'MAINTAINER=\"(.*?)\"'),
    'email': re.compile(r'EMAIL=\"(.*?)\"'),
    # 'download': re.compile(r'DOWNLOAD=\"(.*?)\"'),
    # 'download_64': re.compile(r'DOWNLOAD_x86_64=\"(.*?)\"'),
    # 'md5': re.compile(r'MD5SUM=\"(.*?)\"'),
    # 'md5_64': re.compile(r'MD5SUM_x86_64=\"(.*?)\"'),
    # multiline regex instead
    'download': re.compile(r'DOWNLOAD=\"(.*?)\"', re.S),
    'download_64': re.compile(r'DOWNLOAD_x86_64=\"(.*?)\"', re.S),
    'md5': re.compile(r'MD5SUM=\"(.*?)\"', re.S),
    'md5_64': re.compile(r'MD5SUM_x86_64=\"(.*?)\"', re.S)
}

# do the regexp searches
for key, rx in rx_dict.items():
    match = rx.search(info['contents'])
    if match:
        info[key] = match.group(1)
    else:
        info[key] = None
# catch multiline downloads and quit (for now)
for x in ['download', 'download_64']:
    try:
        dl_lines = len(info[x].split('\n'))
        if dl_lines > 1:
            print(info['fname'] + " has " + str(dl_lines) + " downloads.")
            print("(updating multiline downloads not yet supported).")
            sys.exit()
    except Exception:
        print("Something went wrong.")
        raise

# .info file sanity check (at least PRGNAM, VERSION, and a valid download link)
if info['prgnam'] is None:
    print("No PRGNAM: " + info['fname'])
    sys.exit()
elif info['version'] is None:
    print("No VERSION: " + info['fname'])
    sys.exit()
elif not is_url(info['download']) and not is_url(info['download_64']):
    print("No valid download link: " + info['fname'])
    sys.exit()

# try to update URLs
# TODO: Before downloading check for local file?
if info['download']:
    if info['download'] == 'UNSUPPORTED':
        info['new_download'] = 'UNSUPPORTED'
        info['new_md5'] = ''
    else:
        info['new_download'] = info['download'].replace(info['version'],
                                                        info['new_version'])
        if download(info['new_download']):
            url = urlparse(info['new_download'])
            archive = os.path.basename(url.path)
            info['new_md5'] = md5(archive)
        else:
            info['new_md5'] = ''
            print("Exiting.")
            sys.exit()
else:
    # blank like the originals
    info['new_download'] = info['download']
    info['new_md5'] = info['md5']
# now check for 64-bit details
if info['download_64']:
    if info['download_64'] == 'UNSUPPORTED':
        info['new_download_64'] = 'UNSUPPORTED'
        info['new_md5_64'] = ''
    else:
        info['new_download_64'] = info[
            'download_64'].replace(info['version'],
                                   info['new_version'])
        if download(info['new_download_64']):
            url = urlparse(info['new_download_64'])
            archive = os.path.basename(url.path)
            info['new_md5_64'] = md5(archive)
        else:
            info['new_md5_64'] = ''
            print("Exiting.")
            sys.exit()
else:
    # blank like the originals
    info['new_download_64'] = info['download_64']
    info['new_md5_64'] = info['md5_64']

# (input blanks for info section of type None)
info['new_contents'] = ("PRGNAM=\"" + xstr(info['prgnam']) + "\"\n" +
                        "VERSION=\"" + xstr(info['new_version']) + "\"\n" +
                        "HOMEPAGE=\"" + xstr(info['homepage']) + "\"\n" +
                        "DOWNLOAD=\"" + xstr(info['new_download']) + "\"\n" +
                        "MD5SUM=\"" + xstr(info['new_md5']) + "\"\n" +
                        "DOWNLOAD_x86_64=\"" +
                        xstr(info['new_download_64']) + "\"\n" +
                        "MD5SUM_x86_64=\"" +
                        xstr(info['new_md5_64']) + "\"\n" +
                        "REQUIRES=\"" + xstr(info['requires']) + "\"\n" +
                        "MAINTAINER=\"" + xstr(info['maintainer']) + "\"\n" +
                        "EMAIL=\"" + xstr(info['email']) + "\"\n")
new_info = info['new_contents']

# write it
new_info_fname = info['fname'] + '.new'
try:
    with open(new_info_fname, 'w') as text_file:
        text_file.write(new_info)
except Exception:
    print("Something went wrong, needs fixing.")
    raise

# if there were no changes in the download, then just exit
if md5(info['fname']) == md5(new_info_fname):
    print("\nNo changes to .info file, exiting.")
    sys.exit()
# otherwise compare the differences
print(colour.bold + info['fname'] + colour.end)
diff = subprocess.run(['sdiff', '-l', info['fname'], new_info_fname])

if query_yes_no("\nUpdate the .info file?", "yes"):
    try:
        os.rename(new_info_fname, info['fname'])
    except Exception:
        print("Error renaming file.")
        raise
else:
    print("Abort.")
    os.remove(new_info_fname)
    sys.exit()

# update SlackBuild
try:
    # Read in the file (read-only)
    with open(build_fname, 'r') as file:
        filedata = file.read()
    # Replace the target string
    filedata = filedata.replace('VERSION:-' + info['version'],
                                'VERSION:-' + info['new_version'])
    # Write out the new file
    with open(build_fname + '.new', 'w') as file:
        file.write(filedata)
except Exception:
    print("Something went wrong, needs fixing.")
    raise

# test for changes
if md5(build_fname) == md5(build_fname + '.new'):
    print("\nNo changes made to SlackBuild (no $VERSION?).")
    os.remove(build_fname + '.new')
else:
    # otherwise compare the differences
    print('\n' + colour.bold + build_fname + colour.end)
    diff = subprocess.run(['sdiff', '-l', '-s',
                           build_fname,
                           build_fname + '.new'])
    # prompt to write changes
    if query_yes_no("\nUpdate SlackBuild $VERSION?", "yes"):
        try:
            os.rename(build_fname + '.new', build_fname)
        except Exception:
            print("Something went wrong renaming a file, needs fixing.")
            raise
    else:
        print("Abort.")
        sys.exit()

# BUILD TESTING
# offer to test in a chroot/overlayfs with sbuild
# https://gitlab.com/oshd/sbuild
if not is_tool('sbuild'):
    sys.exit()

# otherwise offer to test the build
if query_yes_no("Test build?", "yes"):
    sbuild = subprocess.run(['sudo', 'sbuild', '-b', build_fname])
else:
    sys.exit()
# check sbuild return code
print("\n" + info['prgnam'] + " " + info['new_version'])
if sbuild.returncode == 0:
    print(colour.green + "Clean build :)" + colour.end)
elif sbuild.returncode == 2:
    print(colour.cyan + "Dirty build :/\n(writes outside of /tmp)." +
          colour.end)
elif sbuild.returncode == 3:
    print(colour.red + "Build failed :(" + colour.end)
elif sbuild.returncode == 4:
    print(colour.red + "Build failed >:(\n(and writes outside of /tmp)." +
          colour.end)
else:
    print("Unknown error.")
